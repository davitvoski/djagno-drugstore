from django import template
from django.contrib.auth.models import User

from messaging.models import Messaging
from usermanagement.models import Member

register = template.Library()


@register.filter(name='has_group')
def has_group(user: User, group_name: str):
    """ Function that checks if user is part of a group"""
    return user.groups.filter(name=group_name).exists()


@register.filter(name='nb_notif')
def nb_notif(user: User) -> int:
    """ Function that return number of unread messages in inbox"""
    mem = Member.objects.get(user=user)
    allnotif = Messaging.objects.filter(to_member=mem, is_read=False)
    return len(allnotif)