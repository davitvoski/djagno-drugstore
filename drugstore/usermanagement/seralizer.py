from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer

from .models import Member


class UserSerializer(ModelSerializer):
    """
    Djagno User Model Serializer
    """
    class Meta:
        model = User
        fields = ('id',"username", "first_name", "last_name", "email")


class MemberSerializer(ModelSerializer):
    """
    Serializer for Member Model that uses UserSerializer
    """
    user = UserSerializer()

    class Meta:
        model = Member
        fields = ('user',    'phone_number')


