from django import http
from django.contrib import messages
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.views import LoginView
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

# #############################
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView, FormView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .forms import *
from django.core.exceptions import ObjectDoesNotExist

from messaging.models import Messaging
from .seralizer import MemberSerializer, UserSerializer


@login_required()
def profile(req):
    """
    View for Profile page - profile.html
    """
    if req.user.is_authenticated:
        try:
            # Get member object
            member = Member.objects.get(user=User.objects.get(username=req.user.username))
            # Return member object
            return render(req, 'usermanagement/profile.html', {'member': member})
        except ObjectDoesNotExist:
            # Member doesn't exist
            return redirect('member_login')
    else:
        return render(req, '')


class UserLoginView(LoginView):
    """
    View for Login - login.html
    """
    template_name = "usermanagement/login.html"
    form = UserLoginForm

    def post(self, req, *args, **kwargs):
        """
        Post method - Login user and direct them to their profile
        """
        form = UserLoginForm(req.POST or None)

        # Check for form validity
        if form.is_valid():
            # Get entered username and password
            username = form['username'].value()
            password = form['password'].value()
            # Get User
            try:
                member = Member.objects.get(user=User.objects.get(username=username))
            except ObjectDoesNotExist as e:
                # Display message when user doesn't exists
                messages.error(req, 'User does not exist')
                return render(req, self.template_name, {'form': form})

            # Check if user is blocked
            if member.is_blocked is False:
                user = authenticate(username=username, password=password)
                try:
                    login(req, user)
                    # Redirect different page if it's a User admin.
                    if req.user.groups.filter(name='User Admin').exists():
                        return redirect('member_manager')

                    # Redirect different page if it's an Item admin.
                    if req.user.groups.filter(name='Item Admin').exists():
                        return redirect('admin_item_page')

                    # Redirect different page if it's a SuperUser admin.
                    if req.user.is_superuser is True:
                        return redirect('su-home')

                    # Redirect if normal member
                    return redirect('member_profile')
                except AttributeError as e:
                    print(e)
                    messages.error(req, 'Wrong Password')
                    return render(req, self.template_name, {'form': form})
            else:
                messages.error(req, 'Account blocked - Contact Support')
                return render(req, self.template_name, {'form': form})

        return render(req, self.template_name, {'form': form, 'page_title': 'Login'})


class SignUpUserView(CreateView):
    """
    View for SignUp Page = signup.html
    Visitor creates an account.
    """
    form_class = UserEditForm
    success_url = reverse_lazy('member_login')
    template_name = 'usermanagement/signup.html'

    def get(self, req, *args, **kwargs):
        """
        GET method - Loads the Form of the page
        """
        reg_form = UserRegistrationForm()
        member_form = MemberForm()
        return render(req, self.template_name, {'reg_form': reg_form, 'member_form': member_form})

    def post(self, req, *args, **kwargs):
        """
        POST method - Check validity of info and registers user.
        """
        reg_form = UserRegistrationForm(req.POST)  # Check here if signup doesn't work
        member_form = MemberForm(req.POST, req.FILES)
        # Check for form validity
        if reg_form.is_valid() and member_form.is_valid():
            # Get information entered
            username = reg_form.cleaned_data.get('username')
            # Save User form
            reg_form.save()

            # Get Member info
            user_pic = member_form.cleaned_data.get('user_pic')
            addr_street = member_form.cleaned_data.get('street')
            addr_city = member_form.cleaned_data.get('city')
            addr_zip_code = member_form.cleaned_data.get('zip_code')
            addr_state = member_form.cleaned_data.get('state')
            addr_country = member_form.cleaned_data.get('country')
            phone = member_form.cleaned_data.get('phone_number')
            member_user = User.objects.get(username=username)

            if user_pic == "" or None or " ":
                # Create User Member with default image
                Member.objects.create(user=member_user, street=addr_street,
                                      city=addr_city, zip_code=addr_zip_code, state=addr_state,
                                      country=addr_country, phone_number=phone)
            else:
                # Create User Member with specific image
                Member.objects.create(user=member_user,user_pic=user_pic, street=addr_street,
                                      city=addr_city, zip_code=addr_zip_code, state=addr_state,
                                      country=addr_country, phone_number=phone)

            messages.success(req, f"Account successfully created for {username}")
            return redirect('member_login')
        else:
            print(member_form.errors)
            messages.error(req, "Information isn't valid")
            return render(req, self.template_name, {'reg_form': reg_form, 'member_form': member_form})

        return render(req, self.template_name, {'reg_form': reg_form, 'member_form': member_form})


# For editing user information
class EditUserClassView(LoginRequiredMixin, UpdateView):
    """
    Edit Profile Page - edit-profile.html
    """
    form_class = UserEditForm
    success_url = reverse_lazy('member_profile')
    template_name = 'usermanagement/edit-profile.html'

    # This is used when initialize
    def get(self, req, *args, **kwargs):
        """
        GET method - Gets user info and displays in form
        """
        # Getting user data to show in form
        user_data = {'email': req.user.email, 'first_name': req.user.first_name,
                     'last_name': req.user.last_name, 'username': req.user.username
                     }
        edit_form = UserEditForm(user_data)
        member_user = Member.objects.get(user=req.user)
        # Getting user_member data to show in form
        member_data = {'street': member_user.street, 'state': member_user.state,
                       'city': member_user.city, 'country': member_user.country,
                       'user_pic': member_user.user_pic.url, 'phone_number': member_user.phone_number,
                       'zip_code': member_user.zip_code}

        member_form = MemberForm(member_data)
        return render(req, self.template_name, {'edit_form': edit_form, 'member_form': member_form})

    def post(self, req, *args, **kwargs):
        """
        POST method - Saves new information of the user
        """
        edit_form = UserEditForm(req.POST)
        member_form = MemberForm(req.POST, req.FILES)

        # Check form validity
        if edit_form.is_valid() and member_form.is_valid():
            # Get member_user
            member_user = Member.objects.get(user=req.user)
            # Change new user data
            req.user.email = edit_form.cleaned_data.get('email')
            req.user.first_name = edit_form.cleaned_data.get('first_name')
            req.user.last_name = edit_form.cleaned_data.get('last_name')
            # Change new member-user data
            new_pic = member_form.cleaned_data.get('user_pic')
            if new_pic is not None or "":
                member_user.user_pic = new_pic
            # Change info
            member_user.street = member_form.cleaned_data.get('street')
            member_user.city = member_form.cleaned_data.get('city')
            member_user.zip_code = member_form.cleaned_data.get('zip_code')
            member_user.state = member_form.cleaned_data.get('state')
            member_user.country = member_form.cleaned_data.get('country')
            member_user.phone_number = member_form.cleaned_data.get('phone_number')

            # Save new Data
            req.user.save()
            member_user.save()
            messages.success(req, "Account Successfully Updated.")
            return redirect('member_profile')

        else:
            print(edit_form.errors)
            print(member_form.errors)
            messages.error(req, "New Information isn't valid")

        return render(req, self.template_name, {'edit_form': edit_form, 'member_form': member_form})


"""
USER ADMIN VIEWS
"""


class UserManagerList(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """
    Lists All user - into the page
    """
    paginate_by = 3
    model = Member
    template_name = 'usermanagement/manager.html'

    def test_func(self):
        # if self.request.user.is_superuser: return True
        return self.request.user.groups.filter(name='User Admin').exists() or self.request.user.groups.filter(name='SuperUser').exists()

    def get(self, req, *args, **kwargs, ):
        """
        GET method - gets all the users and displays 3 by 3
        """
        all_members = Member.objects.all().order_by('id')
        paginator = Paginator(all_members, self.paginate_by)
        page_number = req.GET.get('page')
        page_obj = paginator.get_page(page_number)

        return render(req, self.template_name, {'all_members_page': page_obj, 'no_users': False})


class UserManagerSearchBarView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    """
    Lists A Specific user - into the page
    """
    paginate_by = 3
    model = Member
    template_name = 'usermanagement/manager.html'

    def test_func(self):
        if self.request.user.is_superuser: return True
        return self.request.user.groups.filter(name='User Admin').exists()

    def get(self, req, *args, **kwargs, ):
        """
        GET method - Copy of UserManagerListView but to search a specific user
                     by username
        """
        searched_username = req.GET.get('search-username')
        try:
            # breakpoint()
            members_searched = Member.objects.all().filter(user__username=searched_username)
        except ObjectDoesNotExist as e:
            return render(req, self.template_name, {'all_members_page': [], 'no_users': True})

        if len(members_searched) == 0: return render(req, self.template_name,
                                                     {'all_members_page': [], 'no_users': True})
        # Do not rename all_members_page if you do change also from UserManagerListView
        return render(req, self.template_name, {'all_members_page': members_searched, 'no_users': False})


class UserManagerBlockView(UserPassesTestMixin, UpdateView):
    """
    Block View - Blocks user
    """
    model = Member
    template_name = 'usermanagement/manager.html'
    all_members = Member.objects.all().order_by('id')

    def test_func(self):
        return self.request.user.groups.filter(name='User Admin').exists()

    def post(self, req, *args, **kwargs):
        """
        POST method - check if Admin wants to block or unblock user
        """
        # Block user-member
        if req.POST.get('member_block'):
            member = Member.objects.get(id=kwargs.get('pk'))
            member.is_blocked = True
            member.save()
            return redirect('member_manager')
        # Unblock user-member
        if req.POST.get('member_unblock'):
            member = Member.objects.get(id=kwargs.get('pk'))
            member.is_blocked = False
            member.save()
            return redirect('member_manager')

        return redirect('member_manager')


class UserManagerWarnView(UserPassesTestMixin, LoginRequiredMixin, UpdateView):
    """
    Warn View - Warns user
    """
    model = Member
    template_name = 'usermanagement/manager.html'
    all_members = Member.objects.all().order_by('id')

    def test_func(self):
        """Tests to check if Admin is part of User Admin Group"""
        if self.request.user.is_superuser: return True
        return self.request.user.groups.filter(name='User Admin').exists()

    def post(self, req, *args, **kwargs):
        """
        POST method - User Admin warns a member which sends a message to their inbox
        """
        # Get member
        warned_member = Member.objects.get(id=kwargs.get('pk'))
        # Increment number of warnings and save
        warned_member.warn_count += 1
        warned_member.save()
        # Send a message to Inbox depending on number pf warnings.
        if warned_member.warn_count == 1:
            Messaging.objects.create(from_member=Member.objects.get(user=req.user),
                                     to_member=warned_member,
                                     subject="ADMIN HAS WARNED YOU",
                                     body="YOU HAVE BEEN WARNED - PLEASE FOLLOW THE POLICY")
        if warned_member.warn_count == 2:
            Messaging.objects.create(from_member=Member.objects.get(user=req.user),
                                     to_member=warned_member,
                                     subject="ADMIN HAS WARNED YOU A SECOND TIME",
                                     body="SECOND WARNING - YOUR ACCOUNT WILL BE BLOCKED AFTER A THIRD")
        if warned_member.warn_count == 3:
            Messaging.objects.create(from_member=Member.objects.get(user=req.user),
                                     to_member=warned_member,
                                     subject="ADMIN HAS WARNED YOU A THIRD TIME",
                                     body="YOUR ACCOUNT WAS BLOCKED")
            warned_member.is_blocked = True
            warned_member.warn_count = 0
            warned_member.save()

        return redirect('member_manager')


class UserManagerDeleteView(UserPassesTestMixin, LoginRequiredMixin, DeleteView):
    """
    Delete View  = Deletes user.
    """
    model = User
    template_name = 'usermanagement/user_confirm_delete.html'
    success_url = reverse_lazy('member_manager')

    def test_func(self):
        return self.request.user.groups.filter(name='User Admin').exists()


class UserManagerEditMemberProfileView(UpdateView):
    """
       Edit Profile Page - edit-profile.html
    """
    form_class = UserEditForm
    success_url = reverse_lazy('member_profile')
    template_name = 'usermanagement/user-admin-edit-member.html'

    # This is used when initialize
    def get(self, req, *args, **kwargs):
        """
        GET method - Gets user info and displays in form
        """
        user = User.objects.get(id=kwargs.get('pk'))
        # Getting user data to show in form
        user_data = {'email': user.email, 'first_name': user.first_name,
                     'last_name': user.last_name, 'username': user.username
                     }
        edit_form = UserEditForm(user_data)

        member_user = Member.objects.get(user=user)
        # Getting user_member data to show in form
        member_data = {'street': member_user.street, 'state': member_user.state,
                       'city': member_user.city, 'country': member_user.country,
                       'user_pic': member_user.user_pic.url, 'phone_number': member_user.phone_number,
                       'zip_code': member_user.zip_code}

        member_form = MemberForm(member_data)
        return render(req, self.template_name, {'edit_form': edit_form, 'member_form': member_form})

    def post(self, req, *args, **kwargs):
        """
        POST method - Saves new information of the user
        """
        user_id = int(req.path.split('/')[-3])
        edit_form = UserEditForm(req.POST)
        member_form = MemberForm(req.POST, req.FILES)
        # Check form validity
        if edit_form.is_valid() and member_form.is_valid():
            # Get user and member_user models
            user = User.objects.get(id=user_id)
            member_user = Member.objects.get(user=user)
            # Change new user data
            user.email = edit_form.cleaned_data.get('email')
            user.first_name = edit_form.cleaned_data.get('first_name')
            user.last_name = edit_form.cleaned_data.get('last_name')
            # Change new member-user data
            new_pic = member_form.cleaned_data.get('user_pic')
            if new_pic is not None or "":
                member_user.user_pic = new_pic
            # Change info
            member_user.street = member_form.cleaned_data.get('street')
            member_user.city = member_form.cleaned_data.get('city')
            member_user.zip_code = member_form.cleaned_data.get('zip_code')
            member_user.state = member_form.cleaned_data.get('state')
            member_user.country = member_form.cleaned_data.get('country')
            member_user.phone_number = member_form.cleaned_data.get('phone_number')

            # Save new Data
            user.save()
            member_user.save()
            messages.success(req, "Previous Edit was Successfully Made.")
            return redirect('member_manager')
        else:
            print(edit_form.errors)
            print(member_form.errors)
            messages.error(req, "New Information isn't valid")

        return render(req, self.template_name, {'edit_form': edit_form, 'member_form': member_form})


"""
API VIEWS
"""


@api_view(['GET'])
def api_all_members(req):
    all_members = Member.objects.all()
    serializer_obj = MemberSerializer(all_members, many=True)
    return Response(serializer_obj.data)


@api_view(['GET'])
def api_one_member(req, pk):
    try:
        member = Member.objects.get(user=User.objects.get(id=pk))
        serializer_obj = MemberSerializer(member)
        return Response(serializer_obj.data)
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def api_show_urls(req):
    my_api_urls = {
        'List All Members': 'api/all-members/',
        'List One Member': 'api/<int:pk>/member/',
        'List All Items': 'api/all-items/',
        'List One Item': 'api/<ing:pk>/item/',
        'Create Item - login required': 'api/create-item',
    }
    return Response(my_api_urls)