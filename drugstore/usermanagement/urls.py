"""drugstore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from django.contrib.auth.views import LogoutView

from . import views
from .views import UserManagerList, UserLoginView, EditUserClassView, SignUpUserView, UserManagerDeleteView, \
    UserManagerBlockView, UserManagerWarnView, UserManagerEditMemberProfileView, UserManagerSearchBarView
from django.contrib.auth import views as auth_views

urlpatterns = [

    # Profile Paths
    path("profile/", views.profile, name="member_profile"),
    path("profile/edit", EditUserClassView.as_view(template_name='usermanagement/edit-profile.html'),
         name='member_profile_edit'),

    # Login/Logout Paths
    path("login/", UserLoginView.as_view(template_name="usermanagement/login.html"), name="member_login"),
    path("logout/", LogoutView.as_view(template_name="usermanagement/logout.html"), name="member_logout"),
    path("register/", SignUpUserView.as_view(template_name='usermanagement/signup.html'), name="member_signup"),

    # User Admin Paths
    path("admin-manager", UserManagerList.as_view(template_name='usermanagement/manager.html'), name="member_manager"),
    path("user-admin/<int:pk>/edit-profile/", UserManagerEditMemberProfileView.as_view(), name="user_admin_member_edit_profile"),
    path("<pk>/delete", UserManagerDeleteView.as_view(), name="user_admin_member_delete"),
    path("<pk>/block", UserManagerBlockView.as_view(), name="user_admin_member_block"),
    path("<pk>/warn", UserManagerWarnView.as_view(), name="user_admin_member_warn"),
    path("search-bar/", UserManagerSearchBarView.as_view(), name="user_admin_member_search"),

    # Password Reset Paths
    path("reset-password/",
         auth_views.PasswordResetView.as_view(template_name='usermanagement/password-reset/password_reset_form.html'),
         name="reset_password"),
    path("reset-password-sent/",
         auth_views.PasswordResetDoneView.as_view(
             template_name='usermanagement/password-reset/password_reset_sent.html'),
         name="password_reset_done"),
    path("reset/<uidb64>/<token>/",
         auth_views.PasswordResetConfirmView.as_view(
             template_name='usermanagement/password-reset/password_reset_confirm.html'),
         name="password_reset_confirm"),
    path("reset-passwor-complete/",
         auth_views.PasswordResetCompleteView.as_view(
             template_name='usermanagement/password-reset/password_reset_complete.html'),
         name="password_reset_complete"),


]
