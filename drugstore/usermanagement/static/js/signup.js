'use strict';

let pwd;
let signUpBtn;
let confPwd;
let form;

document.addEventListener('DOMContentLoaded', () => {
    // Get elements
    pwd = document.querySelectorAll('.form-control')[4];
    signUpBtn = document.querySelector('#signup-btn');
    confPwd = document.querySelectorAll('.form-control')[5];
    form = document.querySelector('#signup-form')
    // Add Listeners
    pwd.addEventListener("change", verifyPassword);
    confPwd.addEventListener('change', checkConfirmPassword);
    form.addEventListener('change', checkFormValid);

})

function verifyPassword() {
    let pwdValue = pwd.value;
    //check empty password field
    if (pwdValue == "") {
        document.getElementById("message").innerHTML = "**Please Fill the password!";
        return false;
    }

    //minimum password length validation
    if (pwdValue.length < 8) {
        document.getElementById("message").innerHTML = "**Password length must be at least 8 characters";
        return false;
    }

    //maximum length of password validation
    if (pwdValue.length > 15) {
        document.getElementById("message").innerHTML = "**Password length must not exceed 15 characters";
        return false;
    } else {
        document.getElementById("message").innerHTML = "";
        return true;
    }
}

function checkConfirmPassword() {
    if (pwd.value !== confPwd.value) {
        confPwd.setCustomValidity("Passwords do not match.");
        document.querySelector("#message").innerHTML =
            "**Confirm Password does not match password.";
        return;
    }
    document.getElementById("message").innerHTML = "";
    return true;
}

function checkFormValid() {
    let fname = document.querySelector("#fname");
    let lname = document.querySelector("#lname");
    let email = document.querySelector("#email");
    let phone = document.querySelector('#phone');
    let numberReg =    /[0-9]{3}-[0-9]{3}-[0-9]{4}/;
    let emailReg =  /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    // Check fields
    if (fname.value === "") {
        console.log("First name is not good")
        return;
    }
    if (lname.value === "") {
        console.log("Last name is not good")
        return;
    }
    if (!emailReg.test(email.value)){
        console.log("Email name is not good")
        return;
    }

    if (verifyPassword() === false) {
        console.log("Password is not good")
        return;
    }
    if (confPwd.value !== pwd.value) {
        console.log("Confirm password is not good")
        return;
    }
    if (!numberReg.test(phone.value)) {
        console.log("Phone number is not good")
         document.querySelector("#message").innerHTML =
            "Phone number must be in following format: xxx-xxx-xxxx";
        return;
    }
    console.log("Form is good")
    signUpBtn.disabled = false;
}
