from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms
from django.contrib.auth.models import User
from .models import Member


class UserRegistrationForm(UserCreationForm):
    """
    User Registration Form - Using django.contrib.auth.models.User
    """
    email = forms.EmailField(required=True, )
    first_name = forms.CharField(max_length="50", required=True)
    last_name = forms.CharField(max_length="50", required=True)
    password1 = forms.PasswordInput()
    password2 = forms.PasswordInput()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2',
                  'last_name', 'first_name']


class MemberForm(forms.ModelForm):
    """
    Member Form - for custom Member Model
    Member Form for fields not included in User model
    """
    user_pic = forms.ImageField(required=False)
    street = forms.CharField(max_length=100, required=False)
    city = forms.CharField(max_length=20, required=False)
    state = forms.CharField(max_length=20, required=False)
    zip_code = forms.CharField(max_length=12, required=False)
    country = forms.CharField(max_length=20, required=False)
    phone_number = forms.RegexField(regex='[0-9]{3}-[0-9]{3}-[0-9]{4}', )

    class Meta:
        model = Member
        fields = ['user_pic', 'street', 'city', 'state', 'zip_code', 'country', 'phone_number']


class UserEditForm(UserChangeForm):
    """
    User Edit form - for updating profile Info
    """
    username = forms.CharField(required=False)
    email = forms.EmailField(required=False, )
    first_name = forms.CharField(max_length="50", required=False)
    last_name = forms.CharField(max_length="50", required=False)

    class Meta:
        model = User
        fields = ('email',
                  'last_name', 'first_name')

class UserLoginForm(forms.Form):
    """
    Cusom Login form
    """
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
