from django.core.validators import RegexValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Member(models.Model):
    """
    Member model
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_pic = models.ImageField(upload_to='profile_pics', default="noimage.png")
    address = models.CharField(max_length=1024, null=True)
    street = models.CharField(max_length=100, help_text="Enter address Street.")
    zip_code = models.CharField(max_length=12, help_text="Enter address Zip Code.")
    city = models.CharField(help_text="Enter address City.", max_length=20, )
    state = models.CharField(help_text="Enter address State.", max_length=20, )
    country = models.CharField(help_text="Enter address Country.", max_length=20, )
    phone_regex = RegexValidator(regex=r'[0-9]{3}-[0-9]{3}-[0-9]{4}',
                                 message="Phone number must be entered in the format: 'xxx-xxx-xxxx'.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, null=True)
    is_blocked = models.BooleanField(default=False)
    warn_count = models.IntegerField(default=0, validators=[MaxValueValidator(3)],)

    @property
    def full_name(self):
        """Returns the person's full name."""
        return '%s %s' % (self.user.first_name, self.user.last_name)

    def __str__(self):
        """
        Overriding str method - Saving username as label in /admin
        """
        return f"Member: {self.user.username}"

    def save(self, *args, **kwargs):
        """
        Overriding save method - Saving Full Address
        """
        self.address = f"{self.street} {self.city} ,{self.state}, {self.zip_code}, {self.country}"
        super().save(*args, **kwargs)
