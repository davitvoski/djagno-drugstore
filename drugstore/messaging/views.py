from django.shortcuts import render

from .models import *
from usermanagement.models import *
from django.contrib.auth.models import User


from django.views.generic import *

from django.urls import reverse_lazy, reverse
from django.shortcuts import redirect


from django.contrib.auth.mixins import LoginRequiredMixin

from django.db.models import Count




# Allow user member to send message
class MessagingView(LoginRequiredMixin, CreateView):
    template_name = 'messaging/messaging.html'
    model = Messaging
    fields = ['to_member', 'subject', 'body']
    success_url = reverse_lazy('thank_you_messaging')


    # set value to field not in form when auto created
    def form_valid(self, form):

        # for from_member. from_member cannot be change by the user
        member = Member.objects.get(user=self.request.user)
        form.instance.from_member = member

        return super().form_valid(form)


# Thank you page
def Thank_You_View(req):
    return render(req, 'messaging/thankYouMessaging.html')

# Get all new messages from current login user
def Set_New_Messages_Counter(current_obj, member):

    # dictionary list hold total new messages and total read messages
    is_read = (current_obj.filter(to_member=member).values('is_read').annotate(
        notify=Count('is_read')))

    # set new message is 0 if length of messaging
    if len(is_read) == 0 or is_read[0]['is_read'] is True:
        notify = 0
    else:
        # set total counted  new message
        notify = is_read[0]['notify']
    return notify

# Hold all messages send to current user login
class InboxView(LoginRequiredMixin, ListView):
    model = Messaging
    template_name = 'messaging/inbox.html'

    # change the context value when loading page
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # current messages
        current_obj = Messaging.objects.all()

        # Member object associated to current user
        member = Member.objects.get(user=self.request.user)

        # messaging is sorted by date in  descending order
        context['object_list'] = current_obj.filter(to_member=member).order_by('-pub_date')

        # get all new messages
        context['new_message'] = Set_New_Messages_Counter(current_obj, member)

        return context

# Allow user member to see the content of the message sent to them
class MessageContentView(LoginRequiredMixin, DetailView):
    model = Messaging
    template_name = 'messaging/contentMessage.html'

    def post(self, request, *args, **kwargs):
        current_obj = self.get_object()
        # Mark the viewing message as is_read to True
        current_obj.is_read = True
        current_obj.save()

        list = Messaging.objects.all()

        # Member object associated to current user
        member = Member.objects.get(user=self.request.user)

        # get the total new message to display as notification message number in inbox when redirect back to inbox
        notify = Set_New_Messages_Counter(list, member)

        # messaging is sorted by date in  descending order
        data = {'object_list' : list.filter(to_member=member).order_by('-pub_date'), 'new_message' : notify}

        return render(request, 'messaging/inbox.html', context=data)

# Allow user member to delete message
class DeleteMessageView(LoginRequiredMixin, DeleteView):
    model = Messaging
    template_name = 'messaging/message_confirm_delete.html'
    success_url = reverse_lazy('thank_you_messaging')


# Allow user member to change the message status to read or not read
class EditMessageView(LoginRequiredMixin, UpdateView):
    model = Messaging
    template_name = 'messaging/message_edit_form.html'
    fields = ['is_read']
    success_url = reverse_lazy('thank_you_messaging')
