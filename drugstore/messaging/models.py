from django.db import models
from usermanagement.models import Member

from django.utils import timezone


class Messaging(models.Model):
    from_member = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='current_sender', null=True)
    to_member = models.ForeignKey(Member, on_delete=models.CASCADE, null=False)
    subject = models.CharField(max_length=60, help_text='Enter text for subject', null=False)
    body = models.TextField(max_length=150, help_text='Enter text to send', null=False)
    is_read = models.BooleanField(default=False, help_text='Check true if message is read')
    pub_date = models.DateTimeField(default=timezone.now, null=False)


