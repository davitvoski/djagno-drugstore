"""drugstore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from . import views
from .views import *
urlpatterns = [
    path("compose", MessagingView.as_view(), name="messages"),
    path("thank-you/", views.Thank_You_View, name="thank_you_messaging"),
    path("inbox/", InboxView.as_view(), name="messages_inbox"),
    path("content/<pk>", MessageContentView.as_view(), name="content"),
    path("delete/<pk>", DeleteMessageView.as_view(), name="content_delete"),
    path("edit/<pk>", EditMessageView.as_view(), name="edit_message"),
    path("", InboxView.as_view(), name="messages_inbox"),

]