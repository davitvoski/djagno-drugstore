"""drugstore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, include
from django.contrib.auth.views import LoginView, LogoutView
from . import views
import usermanagement.views as um

urlpatterns = [
    path('su-home', views.su_home, name='su-home'),
    path('mem-list', views.MemberListView.as_view(), name='mem-list'),
    path('admin-list', views.AdminListView.as_view(), name='admin-list'),
    path('new_admin', views.CreateAdminUser.as_view(), name='new_admin'),
    path('audit-log', views.AuditLogView.as_view(), name='audit-log'),
    # assign member to admin group
    path('assign_ia/<int:id>', views.confirm_item_admin, name='assign_ia'),
    path('assign_ua/<int:id>', views.ConfirmUserAdmin.as_view(), name='assign_ua'),
    path('assign_su/<int:id>', views.confirm_superuser_admin, name='assign_su'),
    # remove member from admin group
    path('remove_adm/<int:id>', views.RmvAdmFrmGrp.as_view(), name='remove_adm'),
    # reassignment of member to another admin group
    path('reassign_ia/<int:id>', views.ReassignItemAdmin.as_view(), name='reassign_ia'),
    path('reassign_ua/<int:id>', views.ReassignUserAdmin.as_view(), name='reassign_ua'),
    path('reassign_su/<int:id>', views.ReassignSuperUser.as_view(), name='reassign_su'),
]
