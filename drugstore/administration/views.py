from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import Group
from django.contrib import messages
from django.shortcuts import redirect

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import UpdateView, CreateView
from usermanagement.models import Member
from usermanagement.forms import UserRegistrationForm
from django.views.generic.list import ListView
from django.contrib.auth.models import User
from .models import Audit


# dashboard for superuser
@user_passes_test(lambda you: you.is_superuser)
def su_home(req):
    current_user = req.user
    context_data = {
        'page_title': 'Admin Portal',
        'header_title': f"SUPERUSER DASHBOARD - Welcome {current_user}"
    }
    return render(req, 'administration/su_home.html', context=context_data)


# view that displays all members
# not assigned to admin group
class MemberListView(UserPassesTestMixin, ListView):
    model = Member
    template_name = 'administration/member_list.html'

    def test_func(self):
        return self.request.user.groups.filter(name='SuperUser').exists()

    def get_queryset(self, *args, **kwargs):
        queryset = super(MemberListView, self).get_queryset(*args, **kwargs)
        not_staff = []
        # item_adm_group = Group.
        for mem in queryset:
            if not mem.user.groups.filter(name__in=['User Admin', 'Item Admin', 'SuperUser']).exists():
                not_staff.append(mem)

        return not_staff


# lists all members who are in any
# of the admin groups
class AdminListView(UserPassesTestMixin, ListView):
    model = Member
    template_name = 'administration/admin_list.html'

    def test_func(self):
        return self.request.user.groups.filter(name='SuperUser').exists()

    def get_queryset(self, *args, **kwargs):
        queryset = super(AdminListView, self).get_queryset(*args, **kwargs)
        staff_members = []
        current_user = self.request.user
        for mem in queryset:
            if mem.user.username != current_user.username:
                if mem.user.groups.filter(name__in=['User Admin', 'Item Admin', 'SuperUser']).exists():
                    staff_members.append(mem)

        return staff_members


# view that confirms if you want to
# assign item admin group to the member
@user_passes_test(lambda you: you.is_superuser)
def confirm_item_admin(req, id):
    template_html = 'administration/confirm_assign_admin.html'
    member = Member.objects.get(pk=id)
    context = {'member': member, 'admin_type': 'ITEM ADMIN'}
    current_su = req.user
    if req.method == "POST":
        grp_item_admin = Group.objects.get(name='Item Admin')
        user = member.user
        user.is_staff = True
        user.save()
        grp_item_admin.user_set.add(user)
        grp_item_admin.save()
        messages.success(req, f"{user} has been added to item admin group.")
        append_audit_log(current_su, user, f"{user} added to Item Admin group")
        return redirect('mem-list')

    # if req.method=="POST":
    #     grp_item_admin = Group.objects.get(name='')
    #     pass
    return render(req, template_html, context)


# view that confirms if you want to
# assign user admin group to the member
class ConfirmUserAdmin(UserPassesTestMixin, UpdateView):
    template_html = 'administration/confirm_assign_admin.html'

    def get(self, req, *args, **kwargs):
        member = Member.objects.get(pk=kwargs.get('id'))
        context = {'member': member, 'admin_type': 'USER ADMIN'}
        return render(req, self.template_html, context)

    def post(self, req, *args, **kwargs):
        current_su = req.user
        member = Member.objects.get(pk=kwargs.get('id'))

        grp_user_admin = Group.objects.get(name='User Admin')
        user = member.user
        user.is_staff = True
        user.save()

        grp_user_admin.user_set.add(user)
        grp_user_admin.save()
        messages.success(req, f"{current_su} has added {user} to user admin group.")
        append_audit_log(current_su, user, f"{user} added to User Admin group")

        return redirect('mem-list')

    def test_func(self):
        return self.request.user.groups.filter(name='SuperUser').exists()


# remove's a member's current admin group then
# reassign them to Item Admin group
class ReassignItemAdmin(UserPassesTestMixin, UpdateView):
    template_html = 'administration/confirm_reassign_admin.html'

    def get(self, req, *args, **kwargs):
        member = Member.objects.get(pk=kwargs.get('id'))
        context = {'member': member, 'admin_type': 'ITEM ADMIN'}
        return render(req, self.template_html, context)

    def post(self, req, *args, **kwargs):
        current_su = req.user
        grp_item_admin = Group.objects.get(name='Item Admin')
        user = Member.objects.get(pk=kwargs.get('id')).user
        current_grp = user.groups.all()[0].name

        # remove member's current group
        remove_group(user)
        grp_item_admin.user_set.add(user)
        grp_item_admin.save()

        messages.success(req, f"{current_su} has reassign {user} to item admin group.")
        append_audit_log(current_su, user,
                         f"reassign to group Item Admin from {current_grp}")

        return redirect('admin-list')

    def test_func(self):
        return self.request.user.groups.filter(name='SuperUser').exists()


# remove's a member's current admin group then
# reassign them to User Admin group
class ReassignUserAdmin(UserPassesTestMixin, UpdateView):
    template_html = 'administration/confirm_reassign_admin.html'

    def get(self, req, *args, **kwargs):
        member = Member.objects.get(pk=kwargs.get('id'))
        context = {'member': member, 'admin_type': 'USER ADMIN'}
        return render(req, self.template_html, context)

    def post(self, req, *args, **kwargs):
        current_su = req.user
        grp_user_admin = Group.objects.get(name='User Admin')
        user = Member.objects.get(pk=kwargs.get('id')).user
        # get current group name for audit log
        current_grp = user.groups.all()[0].name

        # remove member's current group
        remove_group(user)
        grp_user_admin.user_set.add(user)
        grp_user_admin.save()

        messages.success(req, f"{current_su} has reassign {user} to item user group.")
        append_audit_log(current_su, user,
                         f"reassign to group User Admin from {current_grp}")

        return redirect('admin-list')

    def test_func(self):
        return self.request.user.groups.filter(name='SuperUser').exists()


# remove's a member's current admin group then
# reassign them to SuperUser group
class ReassignSuperUser(UserPassesTestMixin, UpdateView):
    template_html = 'administration/confirm_reassign_admin.html'

    def get(self, req, *args, **kwargs):
        member = Member.objects.get(pk=kwargs.get('id'))
        context = {'member': member, 'admin_type': 'SuperUser'}
        return render(req, self.template_html, context)

    def post(self, req, *args, **kwargs):
        current_su = req.user
        groups = Group.objects.all().filter(name__in=['User Admin',
                                                      'Item Admin', 'SuperUser'])
        user = Member.objects.get(pk=kwargs.get('id')).user
        # get current group name for audit logs
        current_grp = user.groups.all()[0].name

        for grp in groups:
            grp.user_set.add(user)
            grp.save()

        user.is_superuser = True
        user.save()

        messages.success(req, f"{current_su} has reassign {user} to superuser group.")
        append_audit_log(current_su, user,
                         f"reassign to group SuperUser from {current_grp}")

        return redirect('admin-list')

    def test_func(self):
        return self.request.user.groups.filter(name='SuperUser').exists()


# utility function:
# remove member from admin group
def remove_group(user):
    groups = user.groups.all()
    if user.groups.filter(name='SuperUser').exists():
        for group in groups.filter(name__in=['SuperUser',
                                             'Item Admin', 'User Admin']):
            group.user_set.remove(user)
            group.save()

        user.is_superuser = False
    else:
        # at this point, user should only have one group
        # remove by first index.
        groups[0].user_set.remove(user)


# demote admin to a regular member
# by removing them from admin group(s)
# and remove is_staff status
class RmvAdmFrmGrp(UserPassesTestMixin, UpdateView):
    template_html = 'administration/confirm_remove_admin.html'

    def get(self, req, *args, **kwargs):
        member = Member.objects.get(pk=kwargs.get('id'))
        grp_name = member.user.groups.all()[0].name
        context = {'member': member, 'admin_type': grp_name}
        return render(req, self.template_html, context)

    def post(self, req, *args, **kwargs):
        current_su = req.user
        user = Member.objects.get(pk=kwargs.get('id')).user
        grp_name = user.groups.all()[0]

        remove_group(user)

        user.is_staff = False
        user.save()
        messages.success(req, f"{user} has been removed from {grp_name} and is no longer staff.")
        append_audit_log(current_su, user, f"{user} removed from admin duties")
        return redirect('admin-list')

    def test_func(self):
        return self.request.user.groups.filter(name='SuperUser').exists()


# view that confirms if you want to
# assign user admin group to the member
@user_passes_test(lambda you: you.is_superuser)
def confirm_superuser_admin(req, id):
    current_su = req.user
    template_html = 'administration/confirm_assign_admin.html'
    member = Member.objects.get(pk=id)
    context = {'member': member, 'admin_type': 'SUPER USER'}
    if req.method == "POST":
        groups = Group.objects.all().filter(name__in=['User Admin',
                                                      'Item Admin', 'SuperUser'])
        user = member.user
        user.is_staff = True
        user.is_superuser = True

        for grp in groups:
            grp.user_set.add(user)
            grp.save()

        messages.success(req, f"{user} has been added to super user group.")
        append_audit_log(current_su, user, f"{user} added to SuperUser group")

        return redirect('mem-list')
    return render(req, template_html, context)


# simplified form to create new user admins
class CreateAdminUser(UserPassesTestMixin, CreateView):
    template_html = 'administration/create_new_admin.html'
    success_url = reverse_lazy('su-home')

    def test_func(self):
        return self.request.user.groups.filter(name='SuperUser').exists()

    def get(self, req, *args, **kwargs):
        # load Form for the create admin page
        # from usermanagement.forms
        reg_form = UserRegistrationForm()

        return render(req, self.template_html, {'reg_form': reg_form})

    def post(self, req, *args, **kwargs):

        current_su = req.user
        reg_form = UserRegistrationForm(req.POST)

        if reg_form.is_valid():

            # create new user and member objects
            username = reg_form.cleaned_data.get('username')
            reg_form.save()
            new_admin = User.objects.get(username=username)
            Member.objects.create(user=new_admin)
            if 'user_admin' in req.POST:
                grp_user_admin = Group.objects.get(name='User Admin')
                grp_user_admin.user_set.add(new_admin)
                grp_user_admin.save()
                messages.success(req, f"{current_su} has created new admin user, {username}, for User Admin group.")
                append_audit_log(current_su, new_admin,
                                 f"added new user {new_admin} to User Admin group")

            elif 'item_admin' in req.POST:
                grp_item_admin = Group.objects.get(name='Item Admin')
                grp_item_admin.user_set.add(new_admin)
                grp_item_admin.save()
                messages.success(req, f"{current_su} has created new admin user, {username}, for User Admin group.")
                append_audit_log(current_su, new_admin,
                                 f"added new user {new_admin} to Item Admin group")

            else:
                # if SuperUser add new user to all admin groups
                groups = Group.objects.all().filter(name__in=['User Admin',
                                                              'Item Admin', 'SuperUser'])
                for grp in groups:
                    grp.user_set.add(new_admin)
                    grp.save()
                new_admin.is_superuser = True
                messages.success(req, f"{current_su} has created new admin user, {username}, for User Admin group.")

            new_admin.is_staff = True
            new_admin.save()

            return redirect('su-home')

        else:
            print(reg_form.errors)
            messages.error(req, "Registration information is invalid.")
            return render(req, self.template_html, {'reg_form': reg_form})


# function to log superuser activity to audit logs
def append_audit_log(actor, subject, action):
    Audit.objects.create(actor=actor, subject=subject, action=action)


# listview that displays all the superuser activity
class AuditLogView(UserPassesTestMixin, ListView):
    model = Audit

    def test_func(self):
        return self.request.user.groups.filter(name='SuperUser').exists()
