from django.db import models
from django.contrib.auth.models import User

# Create your models here.

# Audit model for tracking superuser activity
class Audit(models.Model):
    actor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='current_admin', null=False)
    subject = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    action = models.CharField(max_length=80, help_text="enter superusers task", null=False)
    timestamp = models.DateTimeField(auto_now_add=True, null=False)
