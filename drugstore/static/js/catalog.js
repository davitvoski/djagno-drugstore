document.addEventListener('DOMContentLoaded', handler);
function handler(){
   td = document.querySelectorAll('td');
   for(let i = 0; i < td.length; i++ ){
        td[i].addEventListener('mouseover', styleChange);
   }
}

function styleChange(event){

    tr = document.querySelectorAll('tr');
    clearStyle(tr);

    this.parentElement.setAttribute('style', 'box-shadow: 0 15px 25px 0 rgba(0, 0, 0, 0.28),  rgba(0, 0, 0, 0.25) 0px 4px 4px');
}

function clearStyle(tr){
    for(let i = 0; i < tr.length; i++ ){
        tr[i].style.removeProperty("box-shadow");

    }
}