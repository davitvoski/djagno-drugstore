"""drugstore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.conf import settings

from usermanagement import views as user_views
from itemcatalog import views as item_views

urlpatterns = [
    # App urls
    path('admin/', admin.site.urls),
    path("", include('itemcatalog.urls')),
    path("usermanager/", include('usermanagement.urls')),
    path('administration/', include('administration.urls')),
    path("itemcatalog/", include('itemcatalog.urls')),
    path("messaging/", include('messaging.urls')),

    # API Views
    # View urls
    path("api/",user_views.api_show_urls, name="api_urls"),
    # Comes from usermanagement.models
    path("api/all-members/", user_views.api_all_members, name='api_all_members'),
    path("api/<int:pk>/member/", user_views.api_one_member, name='api_one_member'),
    # Comes from itemcatalog.models
    path("api/all-items/", item_views.api_all_items, name='api_all_items'),
    path("api/<int:pk>/item/", item_views.api_one_item, name='api_one_item'),
    path("api/create-item/", item_views.api_create_item, name='api_create_item')
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

