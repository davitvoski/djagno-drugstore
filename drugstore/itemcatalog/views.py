from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import *
from usermanagement.models import *
from django.contrib.auth.models import User

from django.views.generic.list import ListView
from django.views.generic import *
from django.views.generic.edit import *

from django.http import *
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, redirect

from django.db.models import Count

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import *

from .seralizer import ItemSerializer

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


# filter item objects by column name to allow user to search for item
def check_filter_name(filter_name, searched_str):
    if filter_name == 'name':
        return Item.objects.filter(name__icontains=searched_str).order_by('name')
    elif filter_name == 'description':
        return Item.objects.filter(description__icontains=searched_str).order_by('description')
    elif filter_name == 'type':
        return Item.objects.filter(type__icontains=searched_str).order_by('type')
    elif filter_name == 'likes':
        if searched_str == 'Search bar':
            return Item.objects.all().order_by('likes')
        else:
            return Item.objects.filter(likes__icontains=searched_str).order_by('likes')
    elif filter_name == 'price':
        return Item.objects.filter(price__icontains=searched_str).order_by('price')
    else:
        return Item.objects.all()

# user catalog page (main page for all user)
class CatalogUserView(ListView):
    model = Item
    template_name = 'itemCatalog/catalog.html'
    paginate_by = 5
    ordering = ['owner']

    # get new items list when user do a search item
    def post(self, request):
        searched_str = self.request.POST.get('searchBar')
        filter_name = self.request.POST.get('menu')

        list = check_filter_name(filter_name, searched_str)

        data = {'object_list': list}

        return render(request, self.template_name, context=data)



# Catalog item for register member only.
# LoginRequiredMixin  for authenticating
# Using LoginRequiredMixin to prevent non-member to access page
class CatalogMemberView(LoginRequiredMixin, ListView):
    model = Item
    template_name = 'itemCatalog/catalogMembers.html'
    paginate_by = 5
    ordering = ['owner']

    # get new items list when user do a search item
    def post(self, request):
        searched_str = self.request.POST.get('searchBar')
        filter_name = self.request.POST.get('menu')

        list = check_filter_name(filter_name, searched_str)

        data = {'object_list': list}

        return render(request, self.template_name, context=data)

#Catalog page that list only items belonging to the user who is currently login
# LoginRequiredMixin  for authenticating
# Using LoginRequiredMixin to prevent non-member to access page
class CatalogUserItemView(LoginRequiredMixin, ListView):
    model = Item
    template_name = 'itemCatalog/UserItemcatalog.html'
    ordering = ['owner']

    # get new items list when user do a search item
    def post(self, request):
        searched_str = self.request.POST.get('searchBar')
        filter_name =  self.request.POST.get('menu')

        list = check_filter_name(filter_name, searched_str)

        data = { 'object_list': list}

        return render(request, self.template_name, context=data)

# Catalog Item for Item Admin with admin options to manage items
# LoginRequiredMixin  for authenticating
# Using LoginRequiredMixin to prevent non-member to access page
# Using UserPassesTestMixin to only allow Item Admin to access this page
class AdminItemView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Item
    template_name = 'itemCatalog/admin_item_page.html'
    paginate_by = 5
    ordering = ['owner']

    def test_func(self):
        # if self.request.user.is_superuser: return True
        return self.request.user.groups.filter(name='Item Admin').exists() or self.request.user.groups.filter(name='SuperUser').exists()

# Allow admin to edit item in item catalog
class AdminEditView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Item
    template_name = 'itemCatalog/admin_edit_form.html'
    fields = '__all__'
    success_url = reverse_lazy('admin_item_page')

    def test_func(self):
        return self.request.user.groups.filter(name='Item Admin').exists() or self.request.user.groups.filter(name='SuperUser').exists()

# Allow admin to delete item in item catalog
class AdminDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Item
    template_name = 'itemCatalog/admin_confirm_delete.html'
    success_url = reverse_lazy('admin_item_page')

    def test_func(self):
        return self.request.user.groups.filter(name='Item Admin').exists() or self.request.user.groups.filter(name='SuperUser').exists()


# Allow owner of item to edit their item posted in item catalog
class EditView(LoginRequiredMixin, UpdateView):
    model = Item
    template_name = 'itemCatalog/item_edit_form.html'
    # exclude owner to prevent them from changing owner for the item
    fields = ['snapshot', 'name', 'description', 'price', 'type']
    success_url = reverse_lazy('thank_you_item')

    # set value to field not in form when auto created
    def form_valid(self, form):
        # set the member object associated to current user into the form
        member = Member.objects.get(user=self.request.user)
        form.instance.owner = member
        return super().form_valid(form)

# Allow user member to delete their item
class DeleteItemView(LoginRequiredMixin, DeleteView):
    model = Item
    success_url = reverse_lazy('thank_you_item')

# Allow user member to delete their comments
class DeleteCommentView(LoginRequiredMixin, DeleteView):
    model = Comment
    success_url = reverse_lazy('thank_you_item')


# The detail page display the item information, allow user to like, rate, comments, report
class DetailPageView(LoginRequiredMixin, DetailView):
    model = Item
    template_name = 'itemCatalog/detailPage.html'

    # get comments for current item
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_obj = self.get_object()
        context['comments'] = Comment.objects.filter(item=current_obj.id).order_by('-pub_date')

        return context

    # create new comment for current item
    def post(self, request, *args, **kwargs):
        # current item
        current_obj = self.get_object()

        # Member object associated to current user
        member = Member.objects.get(user=self.request.user)

        # find userRate object associated to current item and current user and return a queryset
        userRate_obj = UserRate.objects.filter(item_name=current_obj, current_user=member)

        # default message for the user
        message = 'no message'

        # get UserRate object from inside of queryset
        get_userRate = None
        for user in userRate_obj:
            get_userRate = user

        already_rated = False

        ###########################################
        # when user submit a rate for the item
        if request.POST.get('rate') == 'Submit':
            # reset message for the user
            message = 'no message'

            try:
                # check if user exist in UserRate table
                if userRate_obj.exists()  == True:

                    #update rate column in userRate for this user and this item
                    if  get_userRate.rate == 0:
                        get_userRate.rate = request.POST.get('starRate')
                        get_userRate.save()
                    else:
                        # message for user if they have already rated
                        message = 'You have already rated this item. ' + "You rated: " +  str(get_userRate.rate)
                        already_rated = True
                #create an record for current user and for this item in UserRate table with the rate given
                else:

                    UserRate.objects.create(current_user=member, item_name=current_obj, rate=request.POST.get('starRate'))

                # update rating in item object
                if already_rated == False:
                    message = 'Successfully rated this item.'
                    # update rating in item object
                    result = (UserRate.objects.filter(item_name=current_obj).values('rate').annotate(
                        rated=Count('rate'))).order_by('rated')
                    result_list = list(result)
                    # get the highest total rate occurrence
                    current_obj.rating = result_list[-1]['rate']
                    current_obj.save()
            except:
                # message for user if  rate submit is empty
                message = 'You did not rate this item. Please enter a number.'

        #########################################
        # like the item
        if request.POST.get('likes') == 'Likes':
            # reset message for the user
            message = 'no message'

            if userRate_obj.exists() == True:
                if get_userRate.liked == False:
                    # mark as liked
                    get_userRate.liked = True
                    get_userRate.save()

                else:
                    message = 'You have already liked this item.'

            else:

                # create a UserRate record for this user with this item and mark liked to true
                UserRate.objects.create(current_user=member, item_name=current_obj, liked=True)

            # update this item total likes
            if message != 'You have already liked this item.':
                message = 'Successfully liked this item.'

                current_obj.likes = current_obj.likes + 1
                current_obj.save()

        #####################################################
        # add comment record
        if request.POST.get('addComment') == 'Add Comment':
            if(self.request.POST.get('content') != ''):
                new_comment = Comment.objects.create(user=member, content=self.request.POST.get('content'))
                new_comment.item = current_obj
                new_comment.save()
            else:
                message = 'You did not enter a comment. Commment cannot be empty.'

        ################################################
        # report the item
        if request.POST.get('report') == 'Report':
            message = 'You have reported this item. Admin will be notify.'
            current_obj.report = True
            current_obj.save()

        ###############################################
        # variables to passed to detail_page
        data = {'comments': Comment.objects.filter(item=current_obj.id).order_by('-pub_date'),
                'object': current_obj,
                'message': message}
        return render(request, 'itemCatalog/detailPage.html', context=data)

# Tank you page
def Thank_You_View(req):
    return render(req, 'itemCatalog/thankYouItem.html')

# Allow user member to add item to catalog
class CreateItemView(LoginRequiredMixin,  CreateView):
    template_name = 'itemCatalog/AddItemPage.html'
    model = Item
    # exclude owner to prevent them from changing owner for the item
    fields = ['snapshot', 'name', 'description', 'price', 'type']
    success_url = reverse_lazy('thank_you_item')

    # set value to field not in form when auto created
    def form_valid(self, form):
        member = Member.objects.get(user=self.request.user)
        form.instance.owner = member
        return super().form_valid(form)

#######################################
# API Views
@api_view(['GET'])
def api_all_items(req):
    all_items = Item.objects.all()
    serializer_obj = ItemSerializer(all_items, many=True)
    return Response(serializer_obj.data)


@api_view(['GET'])
def api_one_item(req, pk):
    try:
        item = Item.objects.get(id=pk)
        serializer_obj = ItemSerializer(item)
        return Response(serializer_obj.data)
    except ObjectDoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@login_required
def api_create_item(req):
    if not req.user.is_authenticated:
        return redirect('member_login')

    serializer_obj = ItemSerializer(data=req.data)
    if serializer_obj.is_valid():
        serializer_obj.save()
        return Response(serializer_obj.data, status=status.HTTP_201_CREATED)
    return Response(serializer_obj.errors, status=status.HTTP_400_BAD_REQUEST)
