from rest_framework.serializers import ModelSerializer, FileField

from .models import Item
from usermanagement import seralizer

class ItemSerializer(ModelSerializer):
    # owner = seralizer.MemberSerializer()
    # snapshot = FileField()

    class Meta:
        model = Item
        fields = ("name", "description", "owner", "price", "type")


