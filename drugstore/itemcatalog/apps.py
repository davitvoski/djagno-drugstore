from django.apps import AppConfig


class ItemcatalogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'itemcatalog'
