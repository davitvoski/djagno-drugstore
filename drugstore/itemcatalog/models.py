from django.db import models
from django.core.validators import *

from usermanagement.models import Member
from django.utils import timezone

# Create your models here.
class Item(models.Model):
    snapshot = models.ImageField(upload_to='items_pic', default='noimage.png')
    name = models.CharField(max_length=30, help_text='Enter your item name')
    description = models.CharField(max_length=53, help_text='Enter item description')
    owner = models.ForeignKey(Member, on_delete=models.CASCADE, null=True)
    price = models.FloatField(validators=[MinValueValidator(0)], help_text='Enter item price')
    type = models.CharField(max_length=30, help_text='Enter item type')
    likes = models.IntegerField(default=0)
    report = models.BooleanField(default=False)
    flag = models.BooleanField(default=False)
    rating = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5)]) # take  the average eg: (2+2+5+4) / 4 = 3 (no remainder)


# record user rate on item
class UserRate(models.Model):
    current_user = models.ForeignKey(Member, on_delete=models.CASCADE, null=True)
    item_name = models.ForeignKey(Item, on_delete=models.CASCADE, null=True)
    rate = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5)])
    pub_date_rate = models.DateTimeField(default=timezone.now, null=True)
    liked = models.BooleanField(default=False)
    pub_date_liked = models.DateTimeField(default=timezone.now, null=True)

class Comment(models.Model):
    user = models.ForeignKey(Member, on_delete=models.CASCADE, null=True)
    pub_date = models.DateTimeField(default=timezone.now, null=False)
    content = models.CharField(max_length=53, help_text='Enter a comment')
    item = models.ForeignKey(Item, on_delete=models.CASCADE, null=True)

