# Generated by Django 4.0.4 on 2022-05-07 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('itemcatalog', '0017_alter_item_snapshot'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='snapshot',
            field=models.ImageField(default='noimage.png', upload_to='items_pic'),
        ),
    ]
