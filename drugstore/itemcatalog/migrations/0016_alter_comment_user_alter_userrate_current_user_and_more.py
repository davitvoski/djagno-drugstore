# Generated by Django 4.0.4 on 2022-04-30 20:09
# Generated by Django 4.0.4 on 2022-05-03 18:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('usermanagement', '0004_alter_member_phone_number'),
        ('usermanagement', '0005_member_is_blocked'),
        ('itemcatalog', '0015_alter_comment_pub_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='usermanagement.member'),
        ),
        migrations.AlterField(
            model_name='userrate',
            name='current_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='usermanagement.member'),
        ),
        migrations.AlterField(
            model_name='userrate',
            name='item_name',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='itemcatalog.item'),
        ),
    ]
