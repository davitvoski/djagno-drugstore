"""drugstore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from . import views
from .views import *
urlpatterns = [
    path("", CatalogUserView.as_view(), name="catalog_user"),
    path("member/", CatalogMemberView.as_view(), name="catalog_member"),
    path("user-items/", CatalogUserItemView.as_view(), name="catalog_user_items"),

    path("detail-Page/item/<int:pk>", DetailPageView.as_view(), name="detail_page"),
    path("edit/<pk>", EditView.as_view(), name='edit_page'),
    path("delete/<pk>", DeleteItemView.as_view(), name='delete_page'),
    path("create-item/", CreateItemView.as_view(), name="create_item"),
    path("thank-you/", views.Thank_You_View, name="thank_you_item"),
    path("delete-comment/<pk>", DeleteCommentView.as_view(), name='delete_comment_page'),

    path("admin-item/", AdminItemView.as_view(), name="admin_item_page"),
    path("admin-edit/<pk>", AdminEditView.as_view(), name="admin_edit_page"),
    path("admin-delete/<pk>", AdminDeleteView.as_view(), name="admin_delete_page"),

    path("", CatalogUserView.as_view(), name="catalog"),

]
