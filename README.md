# dw-42022-prj-grp6-hoang

## Project
This project is a drugstore created with the django framework in our 4th semester of a technical Computer Science Program at Dawson College.<br>

## Description
The main goal of this project was to work with the Django framework. This project consists of 4 different applications. <br>
The superUser application dashboard which also has User and Item admin dashboards. A usermanagment application which manages a users <br>
login, signup, logout, profile and edit profile. Another application for messaging between users where each message is sent to the users' inbox.<br> Lastly, an Item catalog page, where users can add, comment, like posts and browse them. <br>
There are also additional features: an api that uses django rest framework to display all users, a single user, all items, a signle item and the creation of an item. <br>
<b>For Teacher - you can use this user that has some interactions: <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	username: josh <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	passowrd: Test1234! </b>


<h2> Link to Depolyment - Heroku: </h2>
	
	https://dw-42022-prj-grp6-hoang.herokuapp.com/

<h2> <b>  Set Up - Locally  </b></h2>
&nbsp; 	1. Fork the repository or download the zip file. <br>
&nbsp; 	2. Open the project in an IDE. <br>
&nbsp;	3. Create a <a href="https://virtualenvwrapper.readthedocs.io/en/latest/">virutal environment</a> <br>
&nbsp;&nbsp;&nbsp;&nbsp;  a. $pip install virtualenvwrapper <br>
&nbsp;&nbsp;&nbsp;&nbsp;  b. $mkvirtualenv [name of environment] <br>
&nbsp; 	4. Activate the virtual environment. <br>
&nbsp;&nbsp;&nbsp;&nbsp; a. $ ./[name of environment]/[scripts|bin]/activate than click enter <br>
&nbsp;  5. Install all package required <br>
&nbsp;&nbsp;&nbsp;&nbsp; a. $pip install -r requirments.txt  <br>
&nbsp; 	6. Runserver Locally <br>
&nbsp;&nbsp;&nbsp;&nbsp; a. $py manage.py runserver<br>
&nbsp;  7. Enjoy! <br>


<h2><b> How to use it </b> </h2>
<h3> <b> Regular Members </b></h3> 
<h4>Create an Account </h4>
&nbsp;&nbsp;&nbsp;&nbsp;<label> [Register](./readmeimage/register.png) - Create an Account according to the requirments. <label>
<h4> Login </h4>
&nbsp;&nbsp;&nbsp;&nbsp;<label> [Login](./readmeimage/login.png) - Login with your account created. <label>
<h4> Logout </h4>
&nbsp;&nbsp;&nbsp;&nbsp;<label> [Logout](./readmeimage/logout.png) - Logout from your account. <label> 
<h4> Forgot password </h4>
&nbsp;&nbsp;&nbsp;&nbsp;<label> [Forgot Password](./readmeimage/forgotpassword.png) - Click forgot password on login menu <label> <br> 
&nbsp;&nbsp;&nbsp;&nbsp; Enter you email and a link for password reset will be sent to the email associated with your account.
<h4> Edit Profile Information </h4>
&nbsp;&nbsp;&nbsp;&nbsp;<label> [Edit Profile](./readmeimage/editprofilebutton.png) - Click Edit Profile in Profile <label> <br> 
&nbsp;&nbsp;&nbsp;&nbsp;  [Edit the details you need](./readmeimage/editprofilebutton.png) and submit.
<h4> Add Item </h4>
&nbsp;&nbsp;&nbsp;&nbsp;<label> [Add Item](./readmeimage/additembutton.png) - Click Add Item in Catalog Page <label> <br> 
&nbsp;&nbsp;&nbsp;&nbsp;  [Add an item with it's details](./readmeimage/additem.png) and add an item.<br> 
&nbsp;&nbsp;&nbsp;&nbsp;  [You can delete and edit your own items by going to My Items.](./readmeimage/myitems.png)  <br> 
<h3> <b> User Admin  </b></h3> 
<h4> [Capabilities](./readmeimage/useradmin.png) </h4>
&nbsp;&nbsp;&nbsp;&nbsp;<label> Delete a Regular Member <label> <br> 
&nbsp;&nbsp;&nbsp;&nbsp;<label> Block a Regular Member <label> <br> 
&nbsp;&nbsp;&nbsp;&nbsp;<label> Warn a Regular Member - 3 warnings and a member gets blocked <label> <br> 
&nbsp;&nbsp;&nbsp;&nbsp;<label> Edit Profile of a Regular Member <label> <br> 
&nbsp;&nbsp;&nbsp;&nbsp;<label> Add a new Member <label> <br> 
<h3> <b> API </b></h3> 
&nbsp;&nbsp;&nbsp;&nbsp;<label> [An API](./readmeimage/api.png) where you can get all members and items or a signle member and item. <br> 
&nbsp;&nbsp;&nbsp;&nbsp; It is also possible to create an item when you are logged in.


<h2><b> Each Application Description </b></h2>
<h3> User Management app </h3>
<p> This app consists of different aspects: <br>
- A Login page where a user logins into their own account. <br>
- A Logout page where a user logout from ther logged account.<br>
- A Register page where a user creates an account to be able to use the websites features.<br>
- A Profile page where a user can see there information.<br>
- A Edit Profile page which is accessed from a button on profile page. A user can update their information on this page.<br>
- A Forgot passowrd page where a user can reset its password if forgotten. The password reset link is sent to the email of the user. <br>



<h3>Item catalog app:</h3>

	Regular User:
		
		The catalog page is the first page the user land on. This page is for all user.
		The user can only search for item posted. 

	Member User:
		
		Member Catalog page:
			
			When the user login or register as a member, they are send to the profile page. On this page, 
			the user can click on the cart icon to go to the catalog member page.
			The catalog member page allow the user to view all item posted. They can search for a specific item.
			The user can add new item. They can delete their own item or edit them. 

		Detail page:		

			When the user click on details buttom, they are send to the detail page where they can view 
			the item detail information. They can also rate the item viewing between 1 to 5. 
			They can like the item as well. If the user find the item inappropriate, they can report the item
			by clicking the report buttom. When the report show true, the admin will see it in the item admin page.
			The user can leave comment for item. They can also delete the comments on item that they have posted.
			If they wish to delete their comments on other item which they are not the owner, they need to send a 
			message, from the messaging app, to the owner of the item to remove them.

		My Items:
		
			When the user click on My Items, it will show a catalog page where it list only items the user have posted
			on the catalog. On this page, the user can search for specific item. They can also add new item, edit
			or delete. These action will be updated in the member catalog page. They can view the detail page by
			clicking on the detail buttom.

	Item Admin:
		  
			When the item admin login, they are sent to the item admin catalog page.
			Every item in the catalog are shown here. And the report and flag columns lets the admin
			review these items. This page let the admin edit any item posted or delete them. 
			If the admin wish to edit report or flag status for an item, they can change it 
			by clicking the edit button.

			The item admin can flag an item by clicking on edit and checked the flag box. When an item is flag, 
			the user will see a message, Flag by Admin: True, next to the item in the item catalog. 
			This will prevent them from edit, delete or view the detail page.

			Item admin can delete item comments by clicking on the cart image on the menu bar 
			to go to the member catalog page then click on the details buttom to go to the detail page. 
			In the detail page, they can delete any comments.

			Item admin can click on item admin buttom in the menu bar to go back to the item admin catalog page.  
	

<h3>Messaging app:</h3>
	
	New Message indicator:
		
		When the user login, they are sent to the profile page. On this page, they can see a number in red next to
		the inbox buttom on the menu bar. This number is an indicator to let the user know how many new messages are in their inbox.
		
	Inbox:
		
		When the user click on the inbox buttom in the menu bar, they are sent to the inbox page.
		The inbox page list all messages that are sent to the current login user.
		When the message are new and recently received, they are list first. When the message is not read, their color are green.
		When a message is read, their color turn black. There is also a colum with a status to indicate 
		if the message is read or the message is new.
		
	notification icon:
	
		There is a notification icon which show the total new messages in the inbox. 
		
	Edit/Delete
		
		The user can delete a message by cliking on the delete buttom.
		The user can click on the edit buttom to change the status of the message between message read or not read.
				
	View message:
	
		The user can view the message by clicking on the underline text in the subject colum. 
		The subject text is underline and when it is clicked, the user sent to a page to view the message.

		The message page shows information about the message and its content. 
		The user can delete this message by clicking on the delete buttom. It will be remove from the inbox.
		If the user click back buttom, this message color in the inbox will turn black and the status will change to message read.

	Compose Message:

		When the user click on the compose message buttom, they are sent to the compose page. 
		On this page, they can send a message to other members. When the user click send buttom the message will be sent.
		If the user click the cancel buttom, the message will be discarded and the user will be back to the inbox page.

	
<h3>Administration app:</h3>

	The administration part of the website by clicking "SuperUser" on the header is only accessible by SuperUser members.
	SuperUsers have the same privileges as User Admin and Item Admin members in addition to sharing their responsibilities as well.
	They're mainly responsible for managing website actors, ranging from registered members to site admins.

	It is important for super users to familiarize themselves with the features documented below:

	Create admin user:

		A forum is displayed on screen where information is filled to create a new admin account for the website.
		Username and both password fields are required and may fill other necessary data if needed.
		Next the user will be assigned to SuperUser, Item Admin or User Admin by clicking the corresponding button.

	Assign admin role to member:

		Lists all existing members of the website and assign a them to one of three admin groups
		by clicking the corresponding button.
		Only members who aren't currently admins are displayed on the page.

	Modify existing admins:

		Allows the super user to view the entire list of admins from all groups and do the following:

			Removes specific member(s) from admin group, turning them into a regular member.

			Reassign member(s) to a different admin group.

	View SuperUser audit log:

		SuperUser activity involving creating new admin user, assigning a member to a group admin,
		removal, etc. are logged. And this is where you can view all SuperUser activity.
